class CreateCharacters < ActiveRecord::Migration[6.0]
  def change
    create_table :characters do |t|
      t.belongs_to :player, null: false, foreign_key: true
      t.string :name
      t.integer :xp

      t.timestamps
    end
  end
end
