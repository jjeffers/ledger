class CreateXpEntries < ActiveRecord::Migration[6.0]
  def change
    create_table :xp_entries do |t|
      t.integer :amount
      t.belongs_to :player, null: false, foreign_key: true
      t.belongs_to :character, null: false, foreign_key: true
      t.boolean :audited
      t.string :comment

      t.timestamps
    end
  end
end
