json.extract! xp_entry, :id, :amount, :player_id, :character_id, :audited, :created_at, :updated_at
json.url xp_entry_url(xp_entry, format: :json)
