json.extract! character, :id, :player_id, :name, :xp, :created_at, :updated_at
json.url character_url(character, format: :json)
