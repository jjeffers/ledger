class XpEntriesController < ApplicationController
  before_action :set_xp_entry, only: [:show, :edit, :update, :destroy]

  # GET /xp_entries
  # GET /xp_entries.json
  def index
    @xp_entries = XpEntry.all
  end

  # GET /xp_entries/1
  # GET /xp_entries/1.json
  def show
  end

  # GET /xp_entries/new
  def new
    @xp_entry = XpEntry.new
  end

  # GET /xp_entries/1/edit
  def edit
  end

  # POST /xp_entries
  # POST /xp_entries.json
  def create
    @xp_entry = XpEntry.new(xp_entry_params)

    respond_to do |format|
      if @xp_entry.save
        format.html { redirect_to @xp_entry, notice: 'Xp entry was successfully created.' }
        format.json { render :show, status: :created, location: @xp_entry }
      else
        format.html { render :new }
        format.json { render json: @xp_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /xp_entries/1
  # PATCH/PUT /xp_entries/1.json
  def update
    respond_to do |format|
      if @xp_entry.update(xp_entry_params)
        format.html { redirect_to @xp_entry, notice: 'Xp entry was successfully updated.' }
        format.json { render :show, status: :ok, location: @xp_entry }
      else
        format.html { render :edit }
        format.json { render json: @xp_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /xp_entries/1
  # DELETE /xp_entries/1.json
  def destroy
    @xp_entry.destroy
    respond_to do |format|
      format.html { redirect_to xp_entries_url, notice: 'Xp entry was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_xp_entry
      @xp_entry = XpEntry.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def xp_entry_params
      params.require(:xp_entry).permit(:amount, :player_id, :character_id, :audited)
    end
end
