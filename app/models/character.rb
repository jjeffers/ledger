class Character < ApplicationRecord
  belongs_to :player
  has_many :xp_entries, dependent: :destroy

  def xp
    xp_entries.sum(:amount)
  end
end
