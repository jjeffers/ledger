require "application_system_test_case"

class XpEntriesTest < ApplicationSystemTestCase
  setup do
    @xp_entry = xp_entries(:one)
  end

  test "visiting the index" do
    visit xp_entries_url
    assert_selector "h1", text: "Xp Entries"
  end

  test "creating a Xp entry" do
    visit xp_entries_url
    click_on "New Xp Entry"

    fill_in "Amount", with: @xp_entry.amount
    check "Audited" if @xp_entry.audited
    fill_in "Character", with: @xp_entry.character_id
    fill_in "Player", with: @xp_entry.player_id
    click_on "Create Xp entry"

    assert_text "Xp entry was successfully created"
    click_on "Back"
  end

  test "updating a Xp entry" do
    visit xp_entries_url
    click_on "Edit", match: :first

    fill_in "Amount", with: @xp_entry.amount
    check "Audited" if @xp_entry.audited
    fill_in "Character", with: @xp_entry.character_id
    fill_in "Player", with: @xp_entry.player_id
    click_on "Update Xp entry"

    assert_text "Xp entry was successfully updated"
    click_on "Back"
  end

  test "destroying a Xp entry" do
    visit xp_entries_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Xp entry was successfully destroyed"
  end
end
