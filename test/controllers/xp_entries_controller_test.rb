require 'test_helper'

class XpEntriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @xp_entry = xp_entries(:one)
  end

  test "should get index" do
    get xp_entries_url
    assert_response :success
  end

  test "should get new" do
    get new_xp_entry_url
    assert_response :success
  end

  test "should create xp_entry" do
    assert_difference('XpEntry.count') do
      post xp_entries_url, params: { xp_entry: { amount: @xp_entry.amount, audited: @xp_entry.audited, character_id: @xp_entry.character_id, player_id: @xp_entry.player_id } }
    end

    assert_redirected_to xp_entry_url(XpEntry.last)
  end

  test "should show xp_entry" do
    get xp_entry_url(@xp_entry)
    assert_response :success
  end

  test "should get edit" do
    get edit_xp_entry_url(@xp_entry)
    assert_response :success
  end

  test "should update xp_entry" do
    patch xp_entry_url(@xp_entry), params: { xp_entry: { amount: @xp_entry.amount, audited: @xp_entry.audited, character_id: @xp_entry.character_id, player_id: @xp_entry.player_id } }
    assert_redirected_to xp_entry_url(@xp_entry)
  end

  test "should destroy xp_entry" do
    assert_difference('XpEntry.count', -1) do
      delete xp_entry_url(@xp_entry)
    end

    assert_redirected_to xp_entries_url
  end
end
